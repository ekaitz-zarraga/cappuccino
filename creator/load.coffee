path        = require 'path'
fs          = require 'fs'
database    = require './database'
Client      = require './client'
Post        = require './post'

class Loader

    constructor: (source_dir, target_dir, opts) ->
        @source_dir = source_dir
        @target_dir = target_dir
        @opts       = opts

        # Load posts one by one
        try
            names = fs.readdirSync source_dir
        catch
            throw new Error "Impossible to read from #{source_dir}"

        @posts = (new Post @source_dir, @target_dir, name for name in names)

        if @opts.client
            # If client directory is not selected use default
            client_dir = if @opts.client_dir then @opts.client_dir  else null
            @client = new Client client_dir, @target_dir

    register_sqlite: ->
        sql         = require 'sql.js'
        try
            fs.mkdir @target_dir
        catch Error
            console.log "#{@target_dir} exists"

        # Create the database from creation script
        db = new sql.Database()
        dbquery = database
        db.run dbquery

        # Save database on file
        buffer = new Buffer db.export()
        @target_db = path.join @target_dir, "metadata.sqlite"
        fs.writeFileSync @target_db, buffer

        for p in @posts
            p.register @target_db

        if @opts.client
            @client.register()

    register: ->
        try
            fs.mkdir @target_dir
        catch Error
            console.log "#{@target_dir} exists"

        # Save database on file
        @target_db = path.join @target_dir, "metadata.json"
        fs.writeFileSync @target_db, "[]"

        for p in @posts
            p.register @target_db

        if @opts.client
            @client.register()

module.exports = Loader
