path       = require 'path'
fs         = require 'fs'
browserify = require 'browserify'
RelPath    = require './relpath'
CleanCSS   = require 'clean-css'


class Client
    ###
    # Creates the Browser part.
    #
    # Creates index.html file.
    # Creates css bundle.
    # Creates javascript bundle.
    # Creates style folder with images/fonts.
    ###

    constructor: (source_dir, target_dir)->
        # TODO source_dir for user stuff
        @source_dir = if source_dir then source_dir else path.join __dirname, '../client'
        @bundlefile = new RelPath target_dir, 'bundle.js'
        @cssfile    = new RelPath target_dir, 'style.css'
        @indexfile  = new RelPath target_dir, 'index.html'

    css: ->
        ###
        # Combine CSS files and minimize them a little
        ###

        # TODO rebase image and font links?
        c = new CleanCSS
            format:
                breaks:
                    afterAtRule: true
                    afterBlockBegins: true
                    afterBlockEnds: true
                    afterCommend: true
                    afterProperty: true
                    afterRuleBegins: true
                    afterRuleEnds: true
                    beforeBlockEnds: true
                    betweenSelectors: false
                indentBy: 2
                indentWith: 'space'
                spaces:
                    aroundSelectorRelation: true
                    beforeBlockBegins: true
                    beforeValue: true

        # TODO at the moment only takes this
        # Let the user introduce his/her stuff and control dependencies
        cssfile =  path.join @source_dir, 'static/css/index.css'
        c.minify [ cssfile ], (error, output)=>
            if error?
                throw new Error "Invalid CSS: #{error}"

            # DEBUG
            console.log """

            CSS File generated:
            -------------------------------------------------------------------
            Spent time:    #{output.stats.timeSpent}
            Original size: #{output.stats.originalSize}
            Minified size: #{output.stats.minifiedSize}

            """

            fs.writeFile @cssfile.abs(), output.styles, (error)=>
                if error then throw error
                @cssDone = true
                if @jsDone
                    @createIndex()

    javascript: ->
        ###
        # Get client part javascript and create a bundle
        ###

        # TODO at the moment only takes this
        # Let the user introduce his/her stuff and control dependencies
        main =  path.join @source_dir, 'main.js'
        b = browserify
            extensions: '.js'
        b.add main
        readable = b.bundle()
        readable.on 'end', ()=>
            bundleFile.end =>
                @jsDone = true
                if @cssDone
                    @createIndex()

        # TODO take output path from Loader
        bundleFile = fs.createWriteStream @bundlefile.abs()

    coffeescript: ->
        ###
        # Get client part coffeescript and create a bundle
        # This is only intended to run in CoffeeScript mode.
        ###

        # TODO at the moment only takes this
        # Let the user introduce his/her stuff and control dependencies
        main =  path.join @source_dir, 'main.coffee'
        coffeify   = require 'coffeeify'
        b = browserify
            extensions: '.coffee'
        b.transform coffeify,
            bare: false
        b.add main
        readable = b.bundle()

        # TODO take output path from Loader
        bundleFile = fs.createWriteStream @bundlefile.abs()
        bundleFile.on 'finish', ()=>
            @jsDone = true
            # DEBUG
            console.log 'JavaScript bundle created'
            if @cssDone
               @createIndex()
        readable.pipe bundleFile

    createIndex: ->
        # TODO handle filenames with Relpath
        indexcontent = """
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <script src='#{@bundlefile.url()}'></script>
            <link rel='stylesheet' href='#{@cssfile.url()}'>
        </head>
        <body></body>
        </html>
        """
        # TODO write index.html to target
        fs.writeFile @indexfile.abs(), indexcontent, (error)=>
            if error then throw error
            console.log "Index file written in #{@indexfile.abs()}"

        # TODO copy all static parts (images, fonts)

    register: ->
        # DEBUG
        console.log 'Creating client'

        # TODO, check this point
        # If the this file is compiled, consider the client as compiled also
        filename = if __filename? then __filename else 'test.coffee' # for testing from the console
        if path.extname(filename) == '.coffee'
            @coffeescript()
        else
            @javascript()
        @css()

module.exports = Client
