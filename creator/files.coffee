path    = require 'path'
fs      = require 'fs'
RelPath = require './relpath'

class File

    ###
    # Each file: minimal class to be handled by Files.
    # Not exported.
    ###

    constructor: (source, target, name)->
        @source    = new RelPath source, name
        @target    = new RelPath target, name

    url: ->
        return @target.url()

    copy: ->
        content = fs.readFileSync @source.abs()
        fs.writeFileSync @target.abs(), content

class Files

    constructor: (source, target, place, post_id)->
        @source    = new RelPath source, place
        @target    = new RelPath target, place
        @postname  = post_id
        @category  = path.basename place

        # TODO check if it's a folder:
        #  - Recursively parse?
        #  - Don't add support?
        filenames = fs.readdirSync @source.abs()
        filenames = ( path.join place, f for f in filenames )
        @files = (new File @source.ref, @target.ref, f for f in filenames)

    register_sqlite: (target_db)->
        sql     = require 'sql.js'
        ###
        # Copy and insert in db
        ###
        fs.mkdirSync @target.abs()

        filebuffer = fs.readFileSync target_db
        db = new sql.Database filebuffer

        for f in @files
            f.copy()
            db.run "
                INSERT INTO files (category, post_id, filename)
                VALUES ('#{@category}', '#{@postname}', '#{f.url()}');
            "

        buffer = new Buffer db.export()
        fs.writeFileSync target_db, buffer

    get_files: ()->
        fs.mkdirSync @target.abs()
        files = []
        for f in @files
            f.copy()
            files.push f.url()
        return files


module.exports = Files
