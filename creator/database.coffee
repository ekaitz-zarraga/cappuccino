#
# Database contains only metadata of the files and a way to keep them in order.
# Real content of the files is maintained in the file itself.
#
# Post categories: 'post', 'theory'...
# File categoires: 'img', 'src', 'pcb'...

module.exports = """
CREATE TABLE IF NOT EXISTS posts(
    id          TEXT PRIMARY KEY,
    title       TEXT,
    short_desc  TEXT,
    author      TEXT,
    date        TEXT,
    category    TEXT,
    filename    TEXT
);

CREATE TABLE IF NOT EXISTS files(
    id          INTEGER PRIMARY KEY,
    category    TEXT,
    post_id     TEXT REFERENCES post(id) ON DELETE CASCADE,
    filename    TEXT
);

CREATE TABLE IF NOT EXISTS rel_post_post(
    post_id     TEXT REFERENCES post(id) ON DELETE CASCADE,
    related_id  TEXT REFERENCES post(id) ON DELETE CASCADE,
    PRIMARY KEY (post_id, related_id)
);
"""
