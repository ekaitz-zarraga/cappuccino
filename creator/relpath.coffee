utils = require 'path'

class RelPath

    constructor: ->
        @ref    = ''
        @place  = ''

        if arguments.length == 1

            init_path = arguments[0]

            if init_path instanceof RelPath
                @ref    = init_path.ref
                @place  = init_path.place
            else
                e = new Error 'If only one argument is provided must be a Path'
                throw e

        else if arguments.length == 2

            @ref    = utils.resolve arguments[0]
            @jump arguments[1]

        else if arguments.length > 2
            e = new Error "Invalid arguments: 0, 1 or 2 expected,
                #{arguments.length} provided"
            throw e

    jump: (path)->
        if utils.isAbsolute path
            @place = utils.relative @ref, path

        else
            @place = utils.join @place, path

    url: ->
        norm = utils.normalize @place
        norm = norm.split utils.sep
        url  = "/#{encodeURI norm.join '/'}"

    abs: ->
        utils.join @ref, @place


module.exports = RelPath
