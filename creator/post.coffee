fs   = require 'fs'
path = require 'path'
yaml = require 'js-yaml'
eol  = (require 'os').EOL
RelPath  = require './relpath'
Files    = require './files'

class Post

    constructor: (source, target, name)->
        @target   = new RelPath target, name
        @source   = new RelPath source, name
        @postname = name
        @parse()

    parse: ->
        filenames = fs.readdirSync @source.abs()
        filenames = ( path.join @source.abs(), f for f in filenames )

        # Get contents of the post, files and metadata
        contents  = filenames.filter (f)-> path.extname(f) == '.md'
        metafiles = filenames.filter (f)-> path.basename(f) == 'Metadata.yaml'
        files     = filenames.filter (f)-> fs.statSync(f).isDirectory()

        # Merge posts and replace {{ROOT}} keyword
        @post = ''
        contents.sort()
        for file in contents
            @post += fs.readFileSync file
        @post = @post.replace /{{\s?ROOT\s?}}/g, @target.url()

        # Check if there's a Metadata file, if not, try to get it from posts
        if metafiles.length == 1
            metafile = metafiles[0]
            @metadata = yaml.safeLoad fs.readFileSync metafile
        else if metafiles.length == 0
            # TODO more intelligent parsing, please
            meta      = @post.split eol
            start     = meta.indexOf '---'
            end       = meta.indexOf '...', start
            @metadata = yaml.safeLoad meta[start+1...end].join eol
            # TODO If no metadata ?
        else
            throw new Error "More than one Metadata file #{@source.abs()}"

        # Parse all the files of the post
        @files = []
        for f in files
            @load_files f

    load_files: (directory)->
        directory = path.relative @source.ref, directory
        @files.push new Files @source.ref, @target.ref, directory, @postname

    register_sqlite: (target_db)->
        sql  = require 'sql.js'

        # Write contents in disc
        fs.mkdirSync @target.abs()
        @target.jump 'Contents.md'
        fs.writeFileSync @target.abs(), @post

        # Register on database
        filebuffer = fs.readFileSync target_db
        db = new sql.Database filebuffer

        m = @metadata
        db.run "
            INSERT INTO posts
            (id, title, short_desc, author, date, category, filename)
            VALUES
            (
                '#{@postname}',
                '#{m.title}',
                '#{m.description}',
                '#{m.author}',
                '#{m.date}',
                '#{m.category}',
                '#{@target.url()}'
            );
        "

        buffer = new Buffer db.export()
        fs.writeFileSync target_db, buffer

        # Copy and register all the files
        for f in @files
            f.register target_db

    register: (target_db)->
        # Write contents in disc
        fs.mkdirSync @target.abs()
        @target.jump 'Contents.md'
        fs.writeFileSync @target.abs(), @post

        # Register on database
        db = JSON.parse fs.readFileSync target_db

        files = {}
        for f in @files
            files[f.category] = f.get_files()

        db.push
            id:         @postname
            title:      @metadata.title
            short_desc: @metadata.description
            author:     @metadata.author
            date:       @metadata.date
            category:   @metadata.category
            tags:       @metadata.tags
            filename:   @target.url()
            files:      files

        db = JSON.stringify db, null, 4
        fs.writeFileSync target_db, db



module.exports = Post
