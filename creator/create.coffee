Loader = require './load'
Parser = (require 'argparse').ArgumentParser

parser = new Parser
    addHelp: true
    description: 'Create the site from specified folder'

parser.addArgument [ '-i', '--input'],
    help: 'Input folder'
    required: true

parser.addArgument [ '-o', '--output'],
    help: 'Output folder'
    required: true

parser.addArgument [ '-c', '--client'],
    help: 'Build client'
    action: 'storeTrue'
    required: false

parser.addArgument [ '-C', '--clientDir'],
    help: 'Custom client directory'
    required: false

parser.addArgument [ '-D', '--dumpClient'],
    help: 'Dump default client in folder and exit, ignores other options'
    required: false

args = parser.parseArgs()

if args.dumpClient
    # TODO make the dump function
    console.log "Dump client and exit"
    process.exit()

l = new Loader args.input,
               args.output,
               client: args.client
               client_dir: args.clientDir
l.register()
