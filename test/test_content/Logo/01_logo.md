# Creating a Logo for PCBs in KiCAD

Hi,

When you start with your electronics you need something to say that that's
yours. Don't you?

It is very common to put a rectangle box in the silkscreen saying the version
of the circuit and some `metadata` more including the author or the company.
That's cool but it's not enough for me.

As you can see here, I really like to put some graphics on the things. We have
a cool logo in here and I wanted to put the same in the PCBs.

So, let's talk about how to make this.


First you need a cool logo for your things. MH Electronics has this one, made
by an amazing young artist:

![][logo]

This logo is not suitable for PCB production because it is very complex. As you
know PCB files definitions are made in Gerber files, which are a 'kind-of'
vectorial drawings (more accurately, they are CNC like instructions). This
means that a simpler logo is needed for this. You can make a new logo or
simplify the main logo you have. I asked the artist to create a simpler logo
version which looks like this:

![][logo_simple]

Now, we need `KiCAD`.

I'm using **KiCAD 4.4**. This post might be old so check it. I expect new
versions make this process easier.

First it's important to prepare the logo for the KiCAD part. Fill all
transparent parts and convert it to Black&White (this is not mandatory but I
recommend it).

Go to KiCAD and run `Bitmap2Component`.

Now the easy part, play with the options and you'll get what you want. Export
it to `pcbnew` component and you'll get a fantastic `.kicad_mod` file to use in
your PCBs.

This process has many limitations and we are going to overcome some of them.

It's complicated to resize the components once you make them even if there are
some tools on the internet. The moment to select the size of the component is
in the conversion, pay attention to the resolution and the size in the top
right corner of the `Bitmap2Component`.

It is impossible to make logos in the copper layers or combining some layers at
the same time. But I did.

##TODO


[logo]:         {{ROOT}}/img/logo.png
[logo_simple]:  {{ROOT}}/img/logo_simple.png
