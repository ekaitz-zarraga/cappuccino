# Cappucino DSSM

Decentralized Static Site Manager (DSSM)

It's a Decentralized and Static reconstruction of MH-Electronics site.

See https://gitlab.com/mh-electronics/CMS
And https://gitlab.com/mh-electronics/MH-Electronics


## Decentralize the whole thang

You can use it in the "normal" way but, come on, decentralize the whole thang.

The idea (see issues) is to make a Decentralized site generator which supports
many protocols.


## Principle

Like other static website generators, *Cappuccino DSSM* creates static files
from the contents and templates created by the user.

Instead of creating HTML pages with navigation, *Cappuccino DSSM* creates a
single-page-app which downloads the contents in an ordered way.

It is a web-page but you can consider it a program which runs in the browser
and downloads contents depending on the user interaction. That's what a
single-page-app is, isn't it?

The cool thing is if you don't like the web interface, you can write a new
client application. You don't need to parse HTML files. You just need to use
the JSON catalog and represent the contents in any way you like.

## Client

*Cappuccino DSSM* comes with a reference client installed which can be dumped
by the user in a folder, extended, and used to create the final site. The
client is not the only way to make this.

## Style

### CSS

Style is based on `Skeleton.css` and some custom stuff.  `Skeleton` is
redistributed inside the project but it's a completely independent project.

> Skeleton is MIT licensed.

### MarkDown to HTML conversion

`Markdown-it` makes the Markdown to HTML conversion.
