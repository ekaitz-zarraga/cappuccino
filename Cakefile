fs            = require 'fs'
path          = require 'path'
os            = require 'os'
eol           = os.EOL
ncp           = (require 'ncp').ncp
mkdirp        = require 'mkdirp'
{spawn, exec} = require 'child_process'

MANIFEST      = 'Cake.Manifest'

option '-o', '--output [DIR]', 'directory for compiled code'

build_binaries = (source, target)->
    ###
    #   Compiles executables. Inserts shebang and chmods in UNIX
    #   Does not accept folders in bin/ directory.
    #       (does anyone put folders there?)
    ###
    exec "coffee -c --no-header -o #{target} #{source}",
        (err, stdout, stderr) ->
            if err
                console.log stderr.trim()
                return

            # TODO remove .js extension on target?
            if os.platform() not in [ 'freebsd', 'linux', 'openbsd' ]
                ok_and_manifest source, target
                return

            targets = fs.readdirSync target
            targets = ( path.join target, t for t in targets)
            for target in targets
                # Insert the Shebang with Node
                file = fs.readFileSync "#{target}", "utf8"
                output = "#!/usr/bin/env node" + eol + eol
                output = output + file
                fs.writeFileSync "#{target}", output

                # Chmod 755
                fs.chmod "#{target}", '755'
                ok_and_manifest source, target


build_sources = (source, target) ->
    ###
    #   Build sources directory.
    ###
    exec "coffee -c --no-header -o #{target} #{source}",
        (err, stdout, stderr) ->
            if err then console.log stderr.trim()
            else
                ok_and_manifest source, target

ok_and_manifest = (source, target) ->
    ###
    #   Show ok message and append to manifest file
    ###
    console.log "#{target}: successfully built from #{source}"
    fs.appendFile MANIFEST, target + eol

delete_recursive = (target) ->
    ###
    #   Delete directory or file recursively
    ###
    if not fs.existsSync target
        return
    if (fs.lstatSync target).isDirectory()
        files = fs.readdirSync target
        delete_recursive path.join target, f for f in files
        fs.rmdirSync target
        console.log "#{target}: Deleted on clean"
    else
        fs.unlinkSync target
        console.log "#{target}: Deleted on clean"

copy_recursive = (source, target) ->
    ###
    #   Copy folder recursively
    ###
    ncp source, target, (err)->
        if err
            console.log "Error while copying #{source} to #{target}"
        ok_and_manifest source, target



task 'build', 'Build coffeescript to javascript', (opt)->
    # Check Manifest file and clean if exists
    if fs.existsSync MANIFEST
        console.log 'Manifest file exists, cleaning first'
        invoke 'clean'
        console.log 'Clean. Start Build'


    dir = opt.output or 'js'
    mkdirp.sync dir

    # Creator folder compilation
    source = "creator"
    target = path.join dir, "creator"
    mkdirp.sync target
    build_sources source, target

    # Index.coffee compilation
    source = "index.coffee"
    target = dir
    mkdirp.sync target
    build_sources source, target

    # Compile binaries
    source = "bin"
    target = path.join dir, "bin"
    mkdirp.sync target
    build_binaries source, target

    # Compile client
    source = "client"
    target = path.join dir, "client"
    mkdirp.sync target
    build_sources source, target

    # Copy client static folder
    source = path.join "client", "static"
    target = path.join dir, "client", "static"
    mkdirp.sync target
    copy_recursive source, target

task 'clean', 'Remove last build', (opt)->
    ###
    #   Remove all in Manifest file and the file itself
    ###
    data = fs.readFileSync MANIFEST, 'utf8'
    files = data.split eol
    delete_recursive file for file in files
    fs.unlinkSync MANIFEST
