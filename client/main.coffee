_         = require 'underscore'
set       = require 'indie-set-core'

qs        = require 'qs'
route     = require 'page'

###
# TODO
# - Make HTTP(s) vs other ways to download
# - Think about user defined templates
#   - get them with the metadata?
#   - make the default first -> use dump client for changes
# - Check file trees
# - Finish this shit
###

DATA = null

download_db = ()->
    ###
    #   Downloads DB from the server via HTTP
    #   If error, tries 5 times.
    #   Returns JSON object or null (when error)
    ###
    db = null

    xhr = new XMLHttpRequest()
    # NOTE: Synchronous at the moment
    # there's nothing to do if we don't have the DB.
    xhr.open 'GET', '/metadata.json', false
    xhr.onload = ()=>
        if xhr.status == 200
            db = JSON.parse xhr.response
        else
            console.log 'Database download error'
    xhr.onerror = (e)=>
        console.log 'Database download error'
    xhr.send()
    return db


init = (context, next)->
    ###
    #   It's run before any callback.
    #
    #   If the user just arrived:
    #       - Downloads the database
    #       - Paints the main template
    #
    #   If the database is not reached:
    #       - Create a fake DB with one error post
    #       - Open that error post -> 500 internal server error
    ###
    if not DATA?
        render 'main_template'

        iters = 5
        until DATA? or iters == 0
            --iters
            DATA = download_db()

        if not DATA?
            DATA =
                id:             '500'
                title:          'Internal Server Error'
                short_desc:     'Impossible to download Database'
                category:       'error'

            route '#!/500'

    next()

index = (ctx, next)->
    console.log 'index!'
    # Parse `ctx.querystring` with qs to choose what to render
    filters = qs.parse ctx.querystring

    d = DATA

    # tags
    if filters.tags? # NOTE: Accept if any of the tags is matched
        filters_tags = [].concat filters.tags # Convert to array if it's not
        d = _.filter d, (el)-> _.some el.tags, (tag)-> tag in filters_tags
    # category
    if filters.category?
        d = _.filter d, (el)-> el.category == filters.category
    # author
    if filters.author?
        d = _.filter d, (el)-> el.author == filters.author

    render 'index', d

post = (ctx, next)->
    console.log 'post!'
    post = _.find DATA, (post)-> post.id == ctx.post

    if not post?
        if not (_.find DATA, (post)-> post.id == 404)?
             render 'default_404'
        route '404'
    else
        render 'post', post

error_500 = (ctx, next)->
    console.log 'INTERNAL SERVER ERROR'
file_category = (ctx, next)->
    console.log 'file category!'
file = (ctx, next)->
    console.log 'file!'

render = (template, data)->
    console.log data


route '*', init
route '/', index
route '/500', error_500
route '/:post', post
route '/:post/:file_category', file_category
route '/:post/:file_category/*', file
route
    hashbang: true
    click:    false

