#!/usr/bin/env coffee

path = require 'path'
fs = require 'fs'
libs = path.join path.dirname( fs.realpathSync( __filename)), '../index'
require libs
